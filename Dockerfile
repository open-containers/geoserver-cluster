ARG BASE_IMAGE_NAME=registry.gitlab.com/open-containers/tomcat-cluster
ARG BASE_IMAGE_TAG=qrt-prometheus

FROM ${BASE_IMAGE_NAME}:${BASE_IMAGE_TAG}


RUN mkdir -p /usr/local/tomcat/webapps/geoserver
RUN mkdir -p /usr/loca/tomcat/work/Catalina/localhost/geoserver

COPY geoserver/ /usr/local/tomcat/webapps/geoserver/
